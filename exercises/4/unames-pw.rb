#! /usr/bin/env ruby2.0

def print_usage
  puts "Usage: #{__FILE__} passwd"
  puts
  puts "Parses the given passwd file and prints the usernames and passwords " 
  puts "in the form username:password"
end

def print_username_passwords_from_file file_path
  unless File.file? file_path
    fail "file not found at #{file_path}"
  end

  unless File.readable? file_path
    fail "file given by #{file_path} isn't readable"
  end

  file = File.open file_path, "r"
  file.each_line do |line|
    sections = line.split ':', 3
    $stdout << sections[0] << ':' << sections[1] << "\n"
  end
end

unless ARGV.length == 1
  print_usage
end

print_username_passwords_from_file ARGV[0]
